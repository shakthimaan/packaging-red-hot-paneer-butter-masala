SOURCE = packaging-red-hot-paneer-butter-masala

all:
	pdflatex $(SOURCE).tex

clean:
	rm -f *.aux *.log *.nav *.out *.pdf *.snm *.toc *.vrb *~ 
