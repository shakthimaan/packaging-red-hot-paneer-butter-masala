Name:           paneer-butter-masala
Version:        2.0
Release:        2%{?dist}
Summary:        Paneer butter masala side dish

Group:          Applications/Productivity
License:        GPLv2+
URL:            http://www.shakthimaan.com
Source0:        http://www.shakthimaan.com/downloads/glv/presentation/%{name}-%{version}.tar.bz2
Patch0:         paneer-fix.patch

#BuildRequires: gcc make
#Requires:      naan

%description
Red hot paneer butter masala is a popular mouth-watering side dish in 
India filled with Indian spices, paneer cubes, cream, tomatoes, and coriander.

%prep
%setup -qn %{name}
%patch0 -p1 -b .c

%build
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}/%{_bindir}

%files
%{_bindir}/paneer-butter-masala
%doc README

%changelog
* Fri May 2 2009 Shakthi Kannan <shakthimaan@gmail.com> 2.0-2
- Fixed Paneer Butter Masala. 

* Fri May 1 2009 Shakthi Kannan <shakthimaan@gmail.com> 2.0-1
- Packaged Paneer Butter Masala. 
