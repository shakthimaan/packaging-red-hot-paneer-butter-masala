Name:           paneer-butter-masala
Version:        2.0
Release:        1%{?dist}
Summary:        Paneer butter masala side dish

Group:          Applications/Productivity
License:        GPLv2+
URL:            http://www.shakthimaan.com
Source0:        http://www.shakthimaan.com/downloads/glv/presentation/%{name}-%{version}.tar.bz2

#BuildRequires: gcc make
#Requires:      naan

%description
Red hot paneer butter masala is a popular mouth-watering side dish in 
India filled with Indian spices, paneer cubes, cream, tomatoes, and coriander.

%prep
%setup -qn %{name}

%build
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}/%{_bindir}

%files
%{_bindir}/paneer-butter-masala
%doc README

%changelog
* Fri May 1 2009 Shakthi Kannan <shakthimaan@gmail.com> 2.0-1
- Packaged Paneer Butter Masala. 
